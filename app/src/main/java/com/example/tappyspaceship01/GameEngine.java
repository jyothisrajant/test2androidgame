package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    private Bitmap image;



    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // represent the TOP LEFT CORNER OF THE GRAPHIC

    Player player;
    Item rainbow;
    Item candy;
    Item candy1;
    Item garbage;
    int lane1XPosition,lane2XPosition,lane3XPosition,lane4XPosition;
    int lane1YPosition,lane2YPosition,lane3YPosition,lane4YPosition; ;

    private ArrayList<Item> candy2 = new ArrayList<Item>();



    // ----------------------------
    // ## GAME STATS
    // ----------------------------

    int lives = 10;
    int score =0;


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.printScreenInfo();

        // put the initial starting position of your player and enemies
        this.player = new Player(getContext(), screenWidth/2+400, screenHeight/2-400);

        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.candy64);
        this.candy=new Item(getContext(),50,600);
        candy.setImage(image);

        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.rainbow64);
        this.rainbow=new Item(getContext(),50,400);
        rainbow.setImage(image);


        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.poop64);
        this.garbage=new Item(getContext(),50,200);
        garbage.setImage(image);

        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.candy64);
        this.candy1=new Item(getContext(),50,0);
        candy1.setImage(image);

        // Setup the initial position of the lanes
        this.lane1XPosition = 50;
        this.lane1YPosition = 800;
        this.lane2XPosition = 50;
        this.lane2YPosition = 600;
        this.lane3XPosition = 50;
        this.lane3YPosition = 400;
        this.lane4XPosition = 50;
        this.lane4YPosition = 200;


    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    int numLoops = 0;
    public void updatePositions() {




        // @TODO: Update position of player based on mouse tap
        if (this.fingerAction == "mousedown") {
            // if mousedown, then move player up
            // Make the UP movement > than down movement - this will
            // make it look like the player is moving up alot
            player.setyPosition(player.getyPosition() - 100);
            player.updateHitbox();
        }
        else if (this.fingerAction == "mouseup") {
            // if mouseup, then move player down
            player.setyPosition(player.getyPosition() + 10);
            player.updateHitbox();
        }

            candy.setxPosition(candy.getxPosition() + 2);
            candy.updateHitbox();
            rainbow.setxPosition(rainbow.getxPosition() + 3);
            rainbow.updateHitbox();
            garbage.setxPosition(garbage.getxPosition() + 4);
            garbage.updateHitbox();
            candy1.setxPosition(candy1.getxPosition() + 5);
            candy1.updateHitbox();


        // DEAL WITH BULLETS

        // Shoot a bullet every (5) iterations of the loop
        if (numLoops % 5  == 0) {
            this.candy.spawnItem();
        }


        // @TODO: Collision detection code

        // detect when item hits the player
        // ---------------------------------


        if (this.player.getHitbox().intersect(this.candy.getHitbox()) == true) {

            this.candy.setxPosition(50);
            this.candy.setyPosition(400);
            this.candy.updateHitbox();

            // increase the game score!
            this.score = this.score + 1;
        }
        if(this.player.getHitbox().intersect(this.candy1.getHitbox()) == true) {

            this.candy1.setxPosition(50);
            this.candy1.setyPosition(200);
            this.candy1.updateHitbox();

            // increase the game score!
            this.score = this.score + 1;
        }
        if (this.player.getHitbox().intersect(this.rainbow.getHitbox()) == true) {

            this.rainbow.setxPosition(50);
            this.rainbow.setyPosition(0);
            this.rainbow.updateHitbox();

            // increase the game score!
            this.score = this.score + 1;
        }




        if(this.player.getHitbox().intersect(this.garbage.getHitbox()) == true) {

            this.garbage.setxPosition(100);
            this.garbage.setyPosition(600);
            this.garbage.updateHitbox();

            // decreases lives!
            this.lives = this.lives -1;
        }


        //collition between wall and item

        if (this.candy.getxPosition() <= 0) {

            this.candy.setxPosition(50);
            this.candy.setyPosition(400);
            this.candy.updateHitbox();

        }
        if(this.candy1.getxPosition() <= 0) {

            this.candy1.setxPosition(50);
            this.candy1.setyPosition(200);
            this.candy1.updateHitbox();

            // increase the game score!
            this.score = this.score + 1;
        }
        if (this.rainbow.getxPosition() <= 0) {

            this.rainbow.setxPosition(50);
            this.rainbow.setyPosition(0);
            this.rainbow.updateHitbox();

            // increase the game score!
            this.score = this.score + 1;
        }




        if(this.garbage.getxPosition() <= 0) {

            this.garbage.setxPosition(100);
            this.garbage.setyPosition(600);
            this.garbage.updateHitbox();

            // decreases lives!
            this.lives = this.lives -1;
        }
    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);


            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);

            // draw player graphic on screen
            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);
            // draw the player's hitbox
            canvas.drawRect(player.getHitbox(), paintbrush);

            // draw the enemy graphic on the screen
            canvas.drawBitmap(candy.getImage(), candy.getxPosition(), candy.getyPosition(), paintbrush);
            // 2. draw the candy's hitbox
            canvas.drawRect(candy.getHitbox(), paintbrush);

            // draw the enemy graphic on the screen
            canvas.drawBitmap(candy1.getImage(), candy1.getxPosition(), candy1.getyPosition(), paintbrush);
            // 2. draw the candy's hitbox
            canvas.drawRect(candy1.getHitbox(), paintbrush);

            // draw the enemy graphic on the screen
            canvas.drawBitmap(rainbow.getImage(), rainbow.getxPosition(), rainbow.getyPosition(), paintbrush);
            // 2. draw the rainbows's hitbox
            canvas.drawRect(rainbow.getHitbox(), paintbrush);

            // draw the enemy graphic on the screen
            canvas.drawBitmap(garbage.getImage(), garbage.getxPosition(), garbage.getyPosition(), paintbrush);
            // 2. draw the garbage's hitbox
            canvas.drawRect(garbage.getHitbox(), paintbrush);


            // DRAW GAME STATS
            // -----------------------------
            paintbrush.setColor(Color.BLACK);
            paintbrush.setTextSize(60);
            canvas.drawText("LIVES = " + lives,
                    800,
                    100,
                    paintbrush
            );
            canvas.drawText("SCORE = " + score,
                    1200,
                    100,
                    paintbrush
            );


            // 2. Draw the lane

            paintbrush.setColor(Color.BLUE);
            this.canvas.drawRect(this.lane1XPosition,
                    this.lane1YPosition,
                    this.lane1XPosition + 1000,     // 800 is width of racket
                    this.lane1YPosition + 20,    // 50 is height of racket
                    paintbrush);
            paintbrush.setColor(Color.WHITE);

            paintbrush.setColor(Color.BLUE);
            this.canvas.drawRect(this.lane2XPosition,
                    this.lane2YPosition,
                    this.lane2XPosition + 1000,     // 800 is width of racket
                    this.lane2YPosition + 20,    // 50 is height of racket
                    paintbrush);
            paintbrush.setColor(Color.WHITE);

            paintbrush.setColor(Color.BLUE);
            this.canvas.drawRect(this.lane3XPosition,
                    this.lane3YPosition,
                    this.lane3XPosition + 1000,     // 800 is width of racket
                    this.lane3YPosition + 20,    // 50 is height of racket
                    paintbrush);
            paintbrush.setColor(Color.WHITE);

            paintbrush.setColor(Color.BLUE);
            this.canvas.drawRect(this.lane4XPosition,
                    this.lane4YPosition,
                    this.lane4XPosition + 1000,     // 800 is width of racket
                    this.lane4YPosition + 20,    // 50 is height of racket
                    paintbrush);
            paintbrush.setColor(Color.WHITE);

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {

            //Log.d(TAG, "Person tapped the screen");
            // User is pressing down, so move player up
            fingerAction = "mousedown";
        }
        else if (userAction == MotionEvent.ACTION_UP) {

            //Log.d(TAG, "Person lifted finger");
            // User has released finger, so move player down
            fingerAction = "mouseup";
        }

        return true;
    }
}
